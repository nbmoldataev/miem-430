import 'package:flutter/material.dart';
import 'package:font_recognizer/src/localization/string_provider.dart';
import 'package:font_recognizer/src/model/recognition_result.dart';
import 'package:font_recognizer/src/utils/colors.dart';

class HistoryItemWidget extends StatelessWidget {
  final RecognitionResult result;

  HistoryItemWidget({
    @required this.result,
  });

  @override
  Widget build(BuildContext context) {
    final color = AppColors.getRandomColorFromPallete();

    return Container(
      height: 100,
      width: 400,
      padding: EdgeInsets.symmetric(horizontal: 32.0, vertical: 8),
      child: Card(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: Container(
                width: double.infinity,
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text('${result.result}',
                        style: TextStyle(
                          color: Colors.white,
                        )),
                  ),
                ),
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      color,
                      color.withOpacity(0.6),
                    ],
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  '${context.appStrings().probability} ${result.value}',
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
