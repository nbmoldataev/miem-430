import 'dart:math';

import 'package:flutter/material.dart';

class AppColors {
  //static Color green = Color(0xFFD0F5CB); // D0F5CB
  static Color red = Color(0xFFBD6482); // BD6482
  static Color blue = Color(0xFF64A6BD); // 64A6BD

  static Color getRandomColorFromPallete() {
    final colors = [
      AppColors.red,
      //AppColors.green,
      AppColors.blue,
    ];

    final random = Random();

    return colors[random.nextInt(2)];
  }
}
