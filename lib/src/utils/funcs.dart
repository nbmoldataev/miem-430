import 'dart:math';

import 'package:flutter/material.dart';
import 'package:font_recognizer/src/localization/app_strings.dart';
import 'package:font_recognizer/src/localization/en_strings.dart';
import 'package:font_recognizer/src/localization/ru_strings.dart';
import 'package:font_recognizer/src/utils/colors.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'constants.dart';

extension Round on double {
  double roundToPrecision(int n) {
    int fac = pow(10, n);
    return (this * fac).round() / fac;
  }
}

TextStyle buildTextMediumStyle({
  Color fontColor = Colors.white,
}) {
  return TextStyle(
    color: fontColor,
    fontWeight: FontWeight.w500,
  );
}

TextStyle buildBigTextStyle() {
  return TextStyle(
    color: Colors.white,
    fontWeight: FontWeight.w700,
    fontSize: 20,
  );
}

Color generateRandomColor(Color excludedColor) {
  var result = AppColors.getRandomColorFromPallete();
  while (result == excludedColor) {
    result = AppColors.getRandomColorFromPallete();
  }
  return result;
}

Future<AppStrings> loadLanguage() async {
  final prefs = await SharedPreferences.getInstance();

  var strings;
  final lng = prefs.getString(LANGUAGE_KEY);

  if (lng == RussianStrings().runtimeType.toString()) {
    strings = RussianStrings();
  } else if (lng == EnglishStrings().runtimeType.toString()) {
    strings = EnglishStrings();
  } else {
    strings = RussianStrings();
  }

  return strings;
}
