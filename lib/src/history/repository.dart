import 'package:flutter/services.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:font_recognizer/src/db/database.dart';
import 'package:font_recognizer/src/model/recognition_result.dart';
import 'package:mailto/mailto.dart';
import 'package:url_launcher/url_launcher.dart';

abstract class BaseHistoryRepository {
  List<RecognitionResult> loadHistory();
}

class LocalHistoryRepository {
  DBProvider db = DBProvider.db;

  LocalHistoryRepository();

  Future saveResult(RecognitionResult result) async {
    try {
      await db.insert(result.toJson());
    } catch (e) {
      print(e);
    }
  }

  Future<List<RecognitionResult>> loadHistory() async {
    try {
      final data = await db.queryAllRows();

      return List.from(data.map((e) => RecognitionResult.fromJson(e)));
    } catch (e) {
      print(e);
      return null;
    }
  }
}

class RemoteHistoryRepository {
  Future<bool> sendEmail() async {
    try {
      final Email email = Email(
        subject: 'Данные о распозновании',
        recipients: ['example@example.com'],
        isHTML: false,
      );

      await FlutterEmailSender.send(email);
      return true;
    } on PlatformException catch (e) {
      return false;
    }
  }

  Future<bool> sendEmailV2() async {
    try {
      final mailtoLink = Mailto(
        subject: 'Данные о распозновании',
        to: ['example@example.com'],
      );

      await launch(mailtoLink.toString());
      return true;
    } catch (e) {
      return false;
    }
  }
}
