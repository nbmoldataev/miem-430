import 'package:font_recognizer/src/model/recognition_result.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DBProvider {
  static const String _TABLE_NAME_KEY = 'history';

  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;

    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  Future initDB() async {
    final documentsDirectory = await getApplicationDocumentsDirectory();
    final path = documentsDirectory.path + "TestDB.db";
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE $_TABLE_NAME_KEY ("
          "id INTEGER PRIMARY KEY,"
          "${RecognitionResult.RESULT_KEY} TEXT,"
          "${RecognitionResult.VALUE_KEY} REAL"
          ")");
    });
  }

  Future<int> insert(Map<String, dynamic> row) async {
    Database db = await database;
    return await db.insert(_TABLE_NAME_KEY, row);
  }

  Future<List<Map<String, dynamic>>> queryAllRows() async {
    Database db = await database;
    return await db.query(_TABLE_NAME_KEY);
  }

  DBProvider._();
  static final DBProvider db = DBProvider._();
}
