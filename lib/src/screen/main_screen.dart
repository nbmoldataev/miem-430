import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_recognizer/src/db/database.dart';
import 'package:font_recognizer/src/history/repository.dart';
import 'package:font_recognizer/src/localization/en_strings.dart';
import 'package:font_recognizer/src/localization/ru_strings.dart';
import 'package:font_recognizer/src/localization/string_provider.dart';
import 'package:font_recognizer/src/ml/model_repository.dart';
import 'package:font_recognizer/src/model/recognition_result.dart';
import 'package:font_recognizer/src/screen/recognition_detail_screen.dart';
import 'package:font_recognizer/src/utils/colors.dart';
import 'package:font_recognizer/src/utils/constants.dart';
import 'package:font_recognizer/src/utils/funcs.dart';
import 'package:font_recognizer/src/widget/expandable_fab.dart';
import 'package:font_recognizer/src/widget/history_item_widget.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tflite/tflite.dart';

class MainScreen extends StatefulWidget {
  final LocalHistoryRepository localStorage;
  final RemoteHistoryRepository remoteStorage;

  MainScreen({
    @required this.localStorage,
    @required this.remoteStorage,
  });

  @override
  State<StatefulWidget> createState() => MainScreenState();
}

class MainScreenState extends State<MainScreen> {
  final modelRepo = ModelRepository();

  LocalHistoryRepository get _localStorage => widget.localStorage;
  RemoteHistoryRepository get _remoteStorage => widget.remoteStorage;
  DBProvider db = DBProvider.db;

  var modelPrediction;

  @override
  void initState() {
    super.initState();
    Future.wait([db.database]);

    _busy = true;

    // loadPredictionModel().then((val) {
    //   setState(() {
    //     _busy = false;
    //   });
    // });
  }

  Future loadPredictionModel() async {
    Tflite.close();

    final model1 = await Tflite.loadModel(
      model: "assets/$MODEL_KEY",
      labels: "assets/labels.txt",
      isAsset: true,
      useGpuDelegate: false,
    );

    print('$model1');
  }

  Future loadDetectionModel() async {
    Tflite.close();

    final model2 = await Tflite.loadModel(
      model: "assets/$DETECTOR_KEY",
      isAsset: true,
      useGpuDelegate: false,
    );
    print('$model2');
  }

  var _answer;
  File _image;
  List _recognitions;
  double _imageHeight;
  double _imageWidth;
  bool _busy = false;

  Future predictImagePicker({@required ImageSource source}) async {
    final imagePicker = ImagePicker();
    final image = await imagePicker.getImage(source: source);
    if (image == null) {
      return;
    }
    setState(() {
      _busy = true;
    });
    predictImage(image);
  }

  Future predictImage(PickedFile image) async {
    if (image == null) {
      return;
    }

    final coords = await Tflite.runModelOnImage(
      path: image.path,
      // imageStd: 127.5,
    );
    // await loadDetectionModel();
    //
    // final coords = await Tflite.runModelOnImage(
    //   path: image.path,
    // );
    //
    await loadPredictionModel();

    final answer = await Tflite.runModelOnBinary(binary: coords[0]);

    final result = RecognitionResult(
      result: answer[0]['label'],
      value: answer[0]['confidence'],
    );

    final data = result.toJson();

    db.insert(data);

    setState(() {
      // _image = image;
      // _answer = answer[0];
      _busy = false;
    });
  }

  var a = ['Velveta', 'Lobster', 'Arial'];
  String x() {
    final index = Random().nextInt(3);
    return a[index];
  }

  @override
  Widget build(BuildContext context) {
    final color1 = AppColors.getRandomColorFromPallete();
    final color2 = generateRandomColor(color1);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.blue,
        title: Text('FontProgrammatic'),
      ),
      drawer: _buildDrawer(context),
      body: Center(
        child: FutureBuilder(
          future: _localStorage.loadHistory(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot == null) {
              return CircularProgressIndicator();
            } else if (snapshot.data == null) {
              return Text(
                context.appStrings().historyIsEmpty,
                style: buildTextMediumStyle(fontColor: Colors.black),
              );
            } else {
              final data = snapshot.data as List;

              return ListView.builder(
                itemCount: data.length,
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => RecognitionDetailView(
                            result: data[index],
                          ),
                        ),
                      );
                    },
                    child: HistoryItemWidget(
                      result: data[index],
                    ),
                  );
                },
              );
            }
          },
        ),
      ),
      floatingActionButton: FutureBuilder(
        future: modelRepo.loadModel(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot == null || snapshot.data == null) {
            return CircularProgressIndicator();
          } else {
            print('loaded model');
            print('>>>> ${snapshot.data}');
            return ExpandableFab(
              distance: 112,
              children: [
                ActionButton(
                  onPressed: () {
                    predictImagePicker(source: ImageSource.camera);
                  },
                  icon: const Icon(Icons.camera_alt),
                ),
                ActionButton(
                  onPressed: () {
                    predictImagePicker(source: ImageSource.gallery);
                  },
                  icon: const Icon(Icons.insert_photo),
                ),
              ],
            );
          }
        },
      ),
    );
  }

  SingleChildScrollView _buildNormalBody() {
    return SingleChildScrollView(
      child: Column(
        children: [
          // SizedBox(
          //   height: 200,
          // ),
          // _buildResultContainer(context, color2, color1),
          // HistoryItemWidget(
          //   result: RecognitionResult(result: 'Lobster', value: 0.8743),
          // ),
          // HistoryItemWidget(
          //   result: RecognitionResult(result: 'Arial', value: 0.7534),
          // ),
        ],
      ),
    );
  }

  Drawer _buildDrawer(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Text(
              context.appStrings().menu,
              style: buildBigTextStyle(),
            ),
            decoration: BoxDecoration(
              color: AppColors.blue,
            ),
          ),
          ListTile(
            title: Text(context.appStrings().changeLanguage),
            onTap: () async {
              final prefs = await SharedPreferences.getInstance();

              var l;
              final s = prefs.getString(LANGUAGE_KEY);
              if (s == EnglishStrings().runtimeType.toString()) {
                l = RussianStrings();
              } else {
                l = EnglishStrings();
              }

              await context
                  .stringProvider(listen: false)
                  .setupCountryStrings(l);
            },
          ),
          ListTile(
            title: Text(context.appStrings().info),
            onTap: () {
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }

  void _showAction(BuildContext context, int index) {
    final _actionTitles = [
      context.appStrings().chooseFromGallery,
      context.appStrings().getFromCamera,
    ];

    showDialog<void>(
      context: context,
      builder: (context) {
        return AlertDialog(
          content: Text(_actionTitles[index]),
          actions: [
            TextButton(
              onPressed: () => Navigator.of(context).pop(),
              child: const Text('CLOSE'),
            ),
          ],
        );
      },
    );
  }
}
