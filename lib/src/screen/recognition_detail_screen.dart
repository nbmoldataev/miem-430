import 'package:flutter/material.dart';
import 'package:font_recognizer/src/history/repository.dart';
import 'package:font_recognizer/src/localization/string_provider.dart';
import 'package:font_recognizer/src/model/recognition_result.dart';
import 'package:font_recognizer/src/utils/colors.dart';
import 'package:font_recognizer/src/utils/funcs.dart';

class RecognitionDetailView extends StatelessWidget {
  final RecognitionResult result;

  const RecognitionDetailView({@required this.result});

  @override
  Widget build(BuildContext _context) {
    final color1 = AppColors.getRandomColorFromPallete();
    final color2 = generateRandomColor(color1);

    return Scaffold(
      appBar: AppBar(
        title: Text(_context.appStrings().details),
        backgroundColor: AppColors.blue,
        actions: [
          PopupMenuButton(
            onSelected: (value) {
              switch (value) {
                case 1:
                  RemoteHistoryRepository().sendEmailV2().then(
                        (isSuccess) => {
                          if (isSuccess)
                            {
                              ScaffoldMessenger.of(_context).showSnackBar(
                                SnackBar(
                                  duration: Duration(seconds: 5),
                                  content: Text(
                                    _context
                                        .appStrings(listen: false)
                                        .noEmailClient,
                                    style: TextStyle(),
                                  ),
                                ),
                              ),
                            }
                        },
                      );

                  break;
                default:
                  print('>>> error');
                  break;
              }
            },
            itemBuilder: (_) => [
              PopupMenuItem(
                  value: 1,
                  child: Text(
                      _context.appStrings(listen: false).sendEmailWithResults)),
            ],
          ),
        ],
      ),
      body: Center(
        child: _buildResultContainer(
          _context,
          color2,
          color1,
        ),
      ),
    );
  }

  Container _buildResultContainer(
      BuildContext context, Color color2, Color color1) {
    return Container(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(16),
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  'Последний результат:',
                  style: buildBigTextStyle(),
                ),
              ),
              Row(
                children: [
                  Text(
                    'Шрифт: ${result.result}',
                    style: buildTextMediumStyle(),
                  ),
                  Spacer(),
                  RichText(
                    text: TextSpan(
                      style: buildTextMediumStyle(),
                      children: [
                        TextSpan(
                          text: 'Вероятность: ',
                        ),
                        TextSpan(
                            text: '${result.value}%',
                            style: TextStyle(color: Colors.lightGreen))
                      ],
                    ),
                  ),
                ],
              ),
              Spacer(),
              Wrap(
                children: [
                  Text(
                    'Если вам кажется что шрифт опознан неверно, то используйте общие рекоммендации по распознаванию:\n',
                    style: buildTextMediumStyle(),
                  ),
                  Text(
                    '1. Сделайте фотографию в освещённом месте\n',
                    style: buildTextMediumStyle(),
                  ),
                  Text(
                    '2. Старайтесь захватить шрифт как можно крупнее, но не слишком близко\n(расстояние вытянутой руки)\n',
                    style: buildTextMediumStyle(),
                  ),
                  Text(
                    '3. Если вам не подошёл шрифт в списке ниже можно увижеть топ похожих\n',
                    style: buildTextMediumStyle(),
                  ),
                ],
              ),
              Spacer(),
            ],
          ),
        ),
      ),
      height: MediaQuery.of(context).size.height / 2,
      width: MediaQuery.of(context).size.height / 2,
      decoration: BoxDecoration(
        color: Color(0xffa29bfe),
        gradient: LinearGradient(
          colors: [
            color2,
            color1,
          ],
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.8),
            spreadRadius: 5,
            blurRadius: 18,
            offset: Offset(0, 1), // changes position of shadow
          ),
        ],
        borderRadius: BorderRadius.circular(16),
      ),
    );
  }
}
