import 'package:flutter/material.dart';
import 'package:font_recognizer/src/model/recognition_result.dart';
import 'package:font_recognizer/src/widget/history_item_widget.dart';

class HistoryScreen extends StatefulWidget {
  final results;

  HistoryScreen({@required this.results});

  @override
  State<StatefulWidget> createState() => HistoryScreenState();
}

class HistoryScreenState extends State<HistoryScreen> {
  List<RecognitionResult> get results => widget.results;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemCount: results.length,
        itemBuilder: (BuildContext context, int index) {
          return HistoryItemWidget(
            result: results[index],
          );
        },
      ),
    );
  }
}
