import 'dart:io';

import 'package:firebase_ml_custom/firebase_ml_custom.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:tflite/tflite.dart';

class ModelRepository {
  FirebaseCustomRemoteModel remoteModel;
  FirebaseModelManager modelManager = FirebaseModelManager.instance;

  Future<File> _loadModelFromFirebase() async {
    try {
      remoteModel = FirebaseCustomRemoteModel('detector');

      final conditions = FirebaseModelDownloadConditions(
        androidRequireWifi: true,
        iosAllowCellularAccess: false,
      );

      print('---');
      await modelManager.download(remoteModel, conditions);
      assert(await modelManager.isModelDownloaded(remoteModel) == true);
      print('>>>');

      var modelFile = await modelManager.getLatestModelFile(remoteModel);
      assert(modelFile != null);
      return modelFile;
    } catch (exception) {
      print('Failed on loading your model from Firebase: $exception');
      print('The program will not be resumed');
      rethrow;
    }
  }

  Future<String> _loadTFLiteModel(File modelFile) async {
    try {
      final appDirectory = await getApplicationDocumentsDirectory();
      final labelsData = await rootBundle.load("assets/labels.txt");
      final labelsFile = await File(appDirectory.path + "/_labels.txt")
          .writeAsBytes(labelsData.buffer
              .asUint8List(labelsData.offsetInBytes, labelsData.lengthInBytes));

      assert(await Tflite.loadModel(
            model: modelFile.path,
            labels: labelsFile.path,
            isAsset: false,
          ) ==
          "success");
      return "Model is loaded";
    } catch (exception) {
      print(
          'Failed on loading your model to the TFLite interpreter: $exception');
      print('The program will not be resumed');
      rethrow;
    }
  }

  Future<String> loadModel() async {
    final modelFile = await _loadModelFromFirebase();
    return await _loadTFLiteModel(modelFile);
  }
}
