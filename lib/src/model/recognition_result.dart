import 'package:flutter/cupertino.dart';

class   RecognitionResult {
  static const String RESULT_KEY = 'result';
  static const String VALUE_KEY = 'value';

  String result;
  double value;

  RecognitionResult({
    index,
    @required this.result,
    @required this.value,
  });

  factory RecognitionResult.fromJson(Map<String, dynamic> json) {
    return RecognitionResult(
      index: json[''],
      result: json[RESULT_KEY],
      value: json[VALUE_KEY],
    );
  }

  Map<String, dynamic> toJson() => {
        RESULT_KEY: result,
        VALUE_KEY: value,
      };
}
