class AppStrings {
  String get chooseFromGallery => 'Выбрать изображение из галереи';
  String get getFromCamera => 'Снять изображение с камеры';
  String get menu => 'Меню';
  String get changeLanguage => 'Сменить язык';
  String get info => 'Информация';

  String get historyIsEmpty => 'История пуста';

  String get noEmailClient => 'На устройсте не установлен почтовый клиент!';

  String get details => 'Детали';

  String get sendEmailWithResults => 'Отправить письмо с результатами';

  String get probability => 'Вероятность';
}
