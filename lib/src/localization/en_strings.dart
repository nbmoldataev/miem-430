import 'package:font_recognizer/src/localization/app_strings.dart';

class EnglishStrings implements AppStrings {
  @override
  // TODO: implement chooseFromGallery
  String get chooseFromGallery => throw UnimplementedError();

  @override
  // TODO: implement getFromCamera
  String get getFromCamera => throw UnimplementedError();

  @override
  String get menu => 'Menu';

  @override
  // TODO: implement changeLanguage
  String get changeLanguage => 'Change language';

  @override
  String get info => 'Info';

  @override
  String get historyIsEmpty => 'History is empty';

  @override
  String get noEmailClient =>
      'There is no email client installed on the device';

  @override
  String get details => 'Detail';

  @override
  String get sendEmailWithResults => 'Send email with the results';

  @override
  String get probability => 'Probability';
}
