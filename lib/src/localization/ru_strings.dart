import 'package:font_recognizer/src/localization/app_strings.dart';

class RussianStrings implements AppStrings {
  @override
  // TODO: implement chooseFromGallery
  String get chooseFromGallery => throw UnimplementedError();

  @override
  // TODO: implement getFromCamera
  String get getFromCamera => throw UnimplementedError();

  @override
  // TODO: implement menu
  String get menu => 'Меню';

  @override
  // TODO: implement changeLanguage
  String get changeLanguage => 'Сменить язык';

  @override
  String get info => 'Информация';

  @override
  String get historyIsEmpty => 'История пуста';

  @override
  String get noEmailClient => 'На устройсте не установлен почтовый клиент!';

  @override
  String get details => 'Детали';

  @override
  String get sendEmailWithResults => 'Отправить письмо с результатами';

  @override
  String get probability => 'Вероятность';
}
