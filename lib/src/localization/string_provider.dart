import 'package:flutter/material.dart';
import 'package:font_recognizer/src/localization/app_strings.dart';
import 'package:font_recognizer/src/utils/constants.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StringProvider extends ChangeNotifier {
  AppStrings appStrings;
  StringProvider(this.appStrings);

  Future setupCountryStrings(AppStrings appStrings) async {
    this.appStrings = appStrings;
    final prefs = await SharedPreferences.getInstance();

    prefs.setString(LANGUAGE_KEY, appStrings.runtimeType.toString());
    notifyListeners();
  }
}

extension WithContext on BuildContext {
  StringProvider stringProvider({bool listen}) =>
      Provider.of<StringProvider>(this, listen: listen ?? true);

  AppStrings appStrings({bool listen}) =>
      Provider.of<StringProvider>(this, listen: listen ?? true).appStrings;
}
