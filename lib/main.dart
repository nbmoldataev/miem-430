import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:font_recognizer/src/history/repository.dart';
import 'package:font_recognizer/src/localization/string_provider.dart';
import 'package:font_recognizer/src/screen/main_screen.dart';
import 'package:font_recognizer/src/utils/funcs.dart';
import 'package:image/image.dart' as img;
import 'package:provider/provider.dart';

main() async {
  WidgetsFlutterBinding.ensureInitialized();
  //FirebaseModelInterpreter interpreter = FirebaseModelInterpreter.instance;
  //FirebaseModelManager manager = FirebaseModelManager.instance;

// //Register Cloud Model
//   manager
//       .registerRemoteModelSource(FirebaseRemoteModelSource(modelName: "test"));
//
// // //Register Local Backup
// //   manager.registerLocalModelSource(FirebaseLocalModelSource(modelName: 'test',  assetFilePath: 'ml/test.tflite');
// //
//
//   var imageBytes = (await rootBundle.load("assets/test.jpg")).buffer;
//   img.Image image = img.decodeJpg(imageBytes.asUint8List());
//   image = img.copyResize(image, width: 224, height: 224);
//
// //The app will download the remote model. While the remote model is being downloaded, it will use the local model.
//   var results = await interpreter.run(
//       remoteModelName: "test",
//       localModelName: "test",
//       inputOutputOptions: FirebaseModelInputOutputOptions([
//         FirebaseModelIOOption(FirebaseModelDataType.FLOAT32, [1, 224, 224, 3])
//       ], [
//         FirebaseModelIOOption(FirebaseModelDataType.FLOAT32, [1, 1001])
//       ]),
//       inputBytes: imageToByteList(image));
//
//   print(results);
  //
  final strings = await loadLanguage();
  final sp = StringProvider(strings);

  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider<StringProvider>(
          create: (_) => sp,
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: MainScreen(
          localStorage: LocalHistoryRepository(),
          remoteStorage: RemoteHistoryRepository(),
        ),
      ),
    ),
  );
}

// // int model
// Uint8List imageToByteList(img.Image image) {
//   var _inputSize = 224;
//   var convertedBytes = new Uint8List(1 * _inputSize * _inputSize * 3);
//   var buffer = new ByteData.view(convertedBytes.buffer);
//   int pixelIndex = 0;
//   for (var i = 0; i < _inputSize; i++) {
//     for (var j = 0; j < _inputSize; j++) {
//       var pixel = image.getPixel(i, j);
//       buffer.setUint8(pixelIndex, (pixel >> 16) & 0xFF);
//       pixelIndex++;
//       buffer.setUint8(pixelIndex, (pixel >> 8) & 0xFF);
//       pixelIndex++;
//       buffer.setUint8(pixelIndex, (pixel) & 0xFF);
//       pixelIndex++;
//     }
//   }
//   return convertedBytes;
// }

// float model
Uint8List imageToByteList(img.Image image) {
  var _inputSize = 224;
  var convertedBytes = Float32List(1 * _inputSize * _inputSize * 3);
  var buffer = Float32List.view(convertedBytes.buffer);
  int pixelIndex = 0;
  for (var i = 0; i < _inputSize; i++) {
    for (var j = 0; j < _inputSize; j++) {
      var pixel = image.getPixel(i, j);
      buffer[pixelIndex] = ((pixel >> 16) & 0xFF) / 255;
      pixelIndex += 1;
      buffer[pixelIndex] = ((pixel >> 8) & 0xFF) / 255;
      pixelIndex += 1;
      buffer[pixelIndex] = ((pixel) & 0xFF) / 255;
      pixelIndex += 1;
    }
  }
  return convertedBytes.buffer.asUint8List();
}
